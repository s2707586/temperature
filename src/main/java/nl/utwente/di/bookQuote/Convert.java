package nl.utwente.di.bookQuote;

public class Convert {

    public double getF(String temp){
        double celsius = Double.parseDouble(temp);
        return celsius * 1.8 + 32;
    }

}
